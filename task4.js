function createNewUser() {
    let firstName = prompt("Введите Ваше имя");
    let lastName = prompt("Введите фамилию");
    let birthday = prompt("Введите дату рождения", "dd.mm.yyyy");
    const newUser = {
      firstName,
      lastName,
      birthday,
      getLogin() {
        return (this.firstName[0] + this.lastName).toLowerCase();
      },
      getPassword() {
        return (
          this.firstName[0].toUpperCase() +
          this.lastName.toLowerCase() +
          this.birthday.slice(6)
        );
      },
      getAge() {
        let dayUser = this.birthday.slice(0, 2);
        let monthUser = this.birthday.slice(3, 5);
        let yearUser = this.birthday.slice(6);
        let today = new Date();
        let day = today.getDate();
        let month = today.getMonth() + 1;
        let year = today.getFullYear();
        let userAge = year - yearUser;
        if (month < monthUser) {
          return userAge - 1;
        } else if (month > monthUser) {
          return userAge;
        } else {
          if (day < dayUser) {
            return userAge - 1;
          } else {
            return userAge;
          }
        }
      },
    };
    return newUser;
  }
  
  let user = createNewUser();
  console.log(user);
  let login = user.getLogin();
  console.log(login);
  let age = user.getAge();
  console.log(age);
  let password = user.getPassword();
  console.log(password);
  